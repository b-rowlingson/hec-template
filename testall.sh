#!/bin/sh

rm -rf Outputs/*
rm -rf Runs/*

qsub -N hello 000-hello.com

qsub -N runRcode 001-Rcode.com

qsub -N t-test -v N="1000",ALT="two.sided",PAIRED="TRUE" 002-t-test.com

qsub -t 1-10 -N tasks 003-run-array.com

# run program1 with some arguments:
qsub -N somename 004-use-args.com 004-program1.R 2.7818 "some more info" 

# run program2 with some other arguments:
qsub -l h_vmem=1.0G -N someothername 004-use-args.com 004-program2.R "2012-01-20" 3.14159


# run example5 using example 4 com script runner

qsub -N normal 004-use-args.com 005-random-opt.R -c 100 -d rnorm
qsub -N uniform 004-use-args.com 005-random-opt.R -c 100 -d runif

