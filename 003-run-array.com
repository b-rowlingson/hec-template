#$ -S /bin/bash

#$ -l h_vmem=1.0G

#$ -e Runs/out.$JOB_NAME.$JOB_ID.$TASK_ID
#$ -o Runs/out.$JOB_NAME.$JOB_ID.$TASK_ID

###  Launch using: qsub -t 1-10 -N tasks 003-run-array.com

source /etc/profile

echo Making folder Outputs/$JOB_NAME/$JOB_ID/$SGE_TASK_ID/
mkdir -p Outputs/$JOB_NAME/$JOB_ID/$SGE_TASK_ID/

module add R

Rscript 003-run-array.R

mv $SGE_STDOUT_PATH Outputs/$JOB_NAME/$JOB_ID/$SGE_TASK_ID/

