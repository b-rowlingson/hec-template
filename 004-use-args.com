#$ -S /bin/bash

#$ -e Runs/out.$JOB_NAME.$JOB_ID
#$ -o Runs/out.$JOB_NAME.$JOB_ID

###  Launch using: 
###
###     qsub -N somename 004-use-args.com 004-run1.R 2.7818 "some more info" 
###
### if you need more memory:
###
###     qsub -l h_vmem=1.0G -N someothername 004-use-args.com 004-run2.R "2012-01-20" 3.14159


source /etc/profile

mkdir -p Outputs/$JOB_NAME/$JOB_ID/

module add R

Rscript "$@"

mv $SGE_STDOUT_PATH Outputs/$JOB_NAME/$JOB_ID/
