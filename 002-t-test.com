#$ -S /bin/bash

#$ -l h_vmem=1.0G

#$ -e Runs/out.$JOB_NAME.$JOB_ID
#$ -o Runs/out.$JOB_NAME.$JOB_ID

###  Launch using: qsub -N somename -v N="1000",ALT="two.sided",PAIRED="TRUE" 002-t-test.com

source /etc/profile

mkdir -p Outputs/$JOB_NAME/$JOB_ID/

module add R

Rscript 002-t-test.R

mv $SGE_STDOUT_PATH Outputs/$JOB_NAME/$JOB_ID/

