#! /bin/sh -x
#
# build script runs from main repo folder

cd Web

# clear out site 
rm -rf public/*

pages="index.md.inc exercises.md.inc"

for page in $pages ; do

    ## fill in includes
    ./include.sh $page > ${page%.inc}
    
    generate-md  --input ./${page%.inc} --output ./public --layout mixu-bootstrap-2col-var
    rm ${page%.inc}

done

cp -r media ./public/

# put files here for links
mkdir public/Testing
cd ..
cp [0-9][0-9][0-9]* Web/public/Testing
cp *.tmpl Web/public/Testing
cp -r Data Web/public/Testing

rm -rf public/*
mkdir -p ./public/
mv Web/public/* ./public/

