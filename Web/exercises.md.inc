
# Introduction to SGE Batch Jobs

## Connecting

Connect to the HEC head node <code>wayland</code> using <code>SSH</code> as described. You will
now get a command prompt.

Although you can run anything on <code>wayland</code> the "head" node that you can run
on the compute nodes, including R, don't use it for anything
long-running. It is only suitable for testing R code on short examples
before launching anything major on the compute nodes.

In this document anything typed on <code>wayland</code> will appear before a `wayland>` prompt.

## Modules

The HEC has a "module" system for add-on software which enables
multiple versions of things like R to happily co-exist. Before using
anything you may need to add the module to your session, for example if you do:

```
wayland> R 
wayland> module add R
wayland> R
```

then R will not run until after the <code>module</code> has run.
This command enables that session to run the default version of R (3.2.0 at time
of writing). Other `module` commands will do things like list available 
software (`module avail`), show currently loaded modules (`module list`),
or remove loaded modules (`module rm R`). Module loading persists until 
you log out.

## Exercises

A set of example job submission files illustrating various tips and tricks
are available in a git code repository. To get them, add the `git` module
and clone the repository. We'll do all this inside a `Testing` folder:

```
wayland> mkdir Testing
wayland> module add git
wayland> git clone --depth 1 https://gitlab.com/b-rowlingson/hec-template.git
```

## Submitting Jobs

See sample exercise 000

```
>>>>../000-hello.com
```

## Running Jobs With R

See sample exercise 001 onwards.

```
>>>>../001-Rcode.com
```

```
>>>>../001-Rcode.R
```


## Task Array Jobs

See sample exercise 003

```
>>>>../003-run-array.com
```
```
>>>>../003-run-array.R
```


## Passing Arguments

Instead of controlling R scripts via environment variables, you can
pass parameters to the script. This makes it easier to test scripts 
on the command line on small examples.

Sample exercise 004 shows how this works.

```
>>>>../004-program1.R
```
```
>>>>../004-program2.R
```
```
>>>>../004-use-args.com
```

## Advanced Argument Passing

The `getopt` package provides advanced argument passing for R scripts. Sample
exercise 005 shows how you can run a script like `Rscript test.R -N 1000 -m linear`,
and get the specified parameters (here "N" and "m") as values in R.



## Batchtools

The Batchtools package provides advanced controls for submitting multiple jobs
to any batch system and collating the results. Sample exercise 006 shows two scripts,
one that launches a number of jobs and one which waits for those to complete 
before collecting the answers into a data frame.

