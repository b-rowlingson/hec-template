# The Lancaster Highly Efficient Computing Cluster

This is a short guide for using the Lancaster University HEC, mostly useful to
people who want to run R code. 

## HEC Hardware

![HEC Architecture Diagram](media/architecture.png)


The HEC is in a large, secure, dedicated server room with cooling and reliable power supplies. The computers
are arranged in several rows of racks of computing nodes (**1**) alongside the disk storage system (**2**). The nodes (**3**)
are independent computers (**4**) with a number of processing cores (**5**) and an amount of memory (RAM, **6**) which is
shared between the cores. The number of cores and the amount of RAM can vary from what is shown here.

## HEC Queueing System

![HEC Architecture Diagram](media/connecting.png)


To run on the HEC, you first connect to the "head" node, <code>wayland</code>, and start a command line session (**1**).
From there you prepare "batch jobs", files of instructions for the queuing system called SGE (Son of Grid Engine). Batch
jobs are submitted to SGE and <code>wayland</code> puts them in a queue (**2**). When sufficient resources (cores, RAM)
are available on the cluster SGE will start the process on the rack servers (**3**). The process will
then read and write from the HEC's disk storage (**4**).

## Accounts

Get an account by sending a request to your departmental computing rep. Give your
name, email address, Lancaster username, and a note on the sort of research
you will be doing. If a PhD student, you should also give your supervisor's name
and make sure they approve your application. For example:

```
Name: Jane Doe
Username: doej
Email: j.doe@lancaster.ac.uk
Research: I will be working on spatial models of tropical
diseases using R and spatial model packages. This will involve
simulation of spatial patterns and modelling.
PhD Supervisor: Prof A N Other
```

## Logging In

The hostname is  `wayland.hec.lancs.ac.uk`

The hostname changes when the system gets major upgrades. This is about the
fifth generation HEC at Lancaster.

Use your central Lancaster username and password as used for logging in to Windows
PCs on campus.

In this document anything typed on the HEC will appear before a `HEC>` prompt. Anything
typed at a prompt on your desktop or laptop will have a `Desktop>` prompt - this mostly
applies to Linux and Mac operating systems.

### Windows

Connect using PuTTY or get Linux-style tools and follow the Linux instructions. 
For example, get `cygwin` and install the `ssh` packages.

I think you can use `Pageant` on Windows as an agent to store your credentials for
SSH logins so you don't need to use passwords. 


### Linux

Connect using `ssh`:
```
 Desktop> ssh wayland.hec.lancs.ac.uk
```

If your username on your local machine is different to the HEC username, then you 
will get an error. Add your username to the address:
```
 Desktop> ssh smith@wayland.hec.lancs.ac.uk
```
To avoid having to type a password every time, copy your public key to wayland:
```
 Desktop> ssh-copy-id wayland.hec.lancs.ac.uk
```
This will ask you for your password in order to copy your key. If it says you 
have "No identities" then use `ssh-keygen` to create one and use the defaults.
Then run `ssh-copy-id` again to transfer it. Connecting with `ssh wayland.hec.lancs.ac.uk`
should now not prompt for a password, but is secure as long as you keep your
private key file (in your `~/.ssh/` folder) secure. Again, if your username is
different on the HEC then use `smith@wayland.hec.lancs.ac.uk` instead of just 
the host name.


### Mac

I think Macs have terminals and `ssh` and all the Linux stuff. 

## File Transfer

### Linux (and Mac?)

Use the command line tools (as before, if your username is different on the
HEC then add `smithxyz@` before the host name).  For example, `scp` for copying files over SSH:
```
 Desktop> scp test.dat wayland.hec.lancs.ac.uk:Testing/
```
Or `rsync` to synchronise whole folder structures:
```
 Desktop> rsync -avz Project/ wayland.hec.lancs.ac.uk:Testing/Project/
```
This works both ways:
```
 Desktop> rsync -avz  wayland.hec.lancs.ac.uk:Testing/Project/ Project/
```
Other options include opening a network connection to wayland via an SSH file
system, then you can edit using your desktop tools transparently.

### Windows

The `cygwin` toolkit can give you the `scp` and `rsync` commands. Otherwise use
`WinSCP`. Set up a profile for connection. Edit files locally and use the 
synchronisation functions to make sure everything is across before running 
on wayland.


## File Storage

There are limits to file storage on the HEC. Quota in your home 
folder is 3Gb, but there are two other places you can put large
data files and outputs.

`storage` gives you 30Gb of quota, but is not backed up.

`scratch` gives you unlimited storage (until the whole device 
is full) but is purged. Files are safe for a couple of weeks,
I think, but you should consider this a temporary store.

You can check your usage on these systems with the `panquota` command:
```
 HEC> panquota 
 Filesystem          Quota      Used   Avail   Use%    # files
 home                3.30G     2.39G   0.91G   72.42     21855 
 storage            33.00G     2.22G  30.78G   6.73      10122 
 scratch                 -    22.68G       -       -       923 
```

Your scratch and storage spaces on the HEC are stored in two environment variables called `global_storage` and
`global_scratch`. If you refer to these variables instead of hard-coding the path to your folder then you will
make your code work more easily between different users. For example in R to dump an object to your storage,
use:

```
storage = Sys.getenv("global_storage")
print(storage)
### [1] "/storage/hpc/41/rowlings"
saveRDS(mtcars, file.path(storage,"cars.RDS"))
```

You should always structure your file storage by making folders for projects and jobs - don't dump
everything at the top level like I did there.

## Running Jobs

In order to make best use of the large, yet limited computing resource of the HEC, you will not
normally be running interactively using the R command line or an interface like `RStudio`. Instead
you have to prepare a text file of commands and send this to the Grid Engine "SGE". These commands
give the system some information about your job and can then run R (or any other program on the
system).

As a quick example, the following file, called `000-hello.com`, tells the engine that it is written using the "bash" command
language, then it reads the standard startup file in `/etc/profile`, and then it writes a message.

```
>>>>../000-hello.com
```

To run this job, you type `qsub -N hello 000-hello.com`. This sends the file to the grid engine, and it will be
placed in a queue. When the system finds a free slot for your job it will run it. The output will appear in a
file in your working folder.

More information on submitting jobs, in increasing complexity, can be found in the [exercises](exercises.html) page
of this documentation.
