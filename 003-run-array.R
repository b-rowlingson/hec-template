
###------------ initialisation

job_id = Sys.getenv("JOB_ID")
job_name= Sys.getenv("JOB_NAME")
task = Sys.getenv("SGE_TASK_ID")

message("Doing task ",task)
message("Job ID ",job_id)
message("Job Name ",job_name)

### This function creates paths that are subfolders of "Outputs"
### with the job-id in so no two jobs ever overwrite each other
outputfile = function(name){
    dir = file.path("Outputs",job_name, job_id)
    dir.create(dir, showWarnings=FALSE, recursive=TRUE)
    file.path(dir, name)
}

N = 10 + (as.integer(task) * 10)
R = runif(N)
S = sum(R)
message("Sum of ",N," runifs is ",S)
